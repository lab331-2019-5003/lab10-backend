package se331.lab.rest.entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Lecturer {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    Long id;
    String name;
    String surname;
    @OneToMany(mappedBy = "advisor")
    @JsonManagedReference
    @Builder.Default
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    List<Student> advisees = new ArrayList<>();
    @OneToMany(mappedBy = "lecturer")
    @JsonManagedReference
    @Builder.Default
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    List<Course> courses = new ArrayList<>();
}
