package se331.lab.rest.dto;

import lombok.*;
import se331.lab.rest.entity.Lecturer;
import se331.lab.rest.entity.Student;

import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class CourseDto {
    Long id;
    String courseId;
    String courseName;
    Lecturer lecturer;
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    List<StudentDto> students = new ArrayList<>();

}
